import java.util.Arrays;
import java.util.Collections;
import java.util.Scanner;

public class ArrayManipulation {
    public static void main(String[] args) {
        int[] numbers = { 5, 8, 3, 2, 7 };
        String[] names = { "alice", "Bob", "Charlie", "David" };
        double[] values = new double[4];
        System.out.println("the elements of the 'numbers'");
        for (int i = 0; i < numbers.length; i++) {
            System.out.println(numbers[i]);
        }
        System.out.println("the elements of the 'name'");
        for (int i = 0; i < names.length; i++) {
            System.out.println(names[i]);
        }
        for (int i = 0; i < values.length; i++) {
            Scanner kb = new Scanner(System.in);
            System.out.println("input values : ");
            values[i] = kb.nextDouble();
        }

        int sum = Arrays.stream(numbers).sum();
        System.out.println("the sum of all eleents in the 'numbers' : " + sum);

        Arrays.sort(values);
        System.out.println("max = " + values[values.length - 1]);

        String[] reverseNames = { "alice", "Bob", "Charlie", "David" };
        Collections.reverse(Arrays.asList(reverseNames));
        System.out.println("Reversed Array:" + Arrays.asList(reverseNames));
    }

}
